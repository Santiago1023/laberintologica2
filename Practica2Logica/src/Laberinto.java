import java.util.ArrayList;
import java.util.Random;
import java.util.Stack;

public class Laberinto {

    public static void main(String[] args) {
        // matriz del enunciado de la practica
        // comentario número 2
        //comentario 3
        int[][] M = {{1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
                     {1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1},
                     {1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1},
                     {1, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 1},
                     {1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1},
                     {1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1},
                     {1, 1, 0, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1},
                     {1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1},
                     {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1}
        };
        // crear una varuable para hacer el llamado al metodo que haya la solucion
        Laberinto santi = new Laberinto();
        Stack <Posicion> camino = santi.caminoLaberinto(0, 1, 8, 11, 9, 13, M);

        //camino es la pila que retorna el metodo con las posiciones del camino y con el while lo vamos a ir mostrando uno a uno
        while(! camino.isEmpty()){
            Posicion x = camino.pop();
            System.out.println("("+ x.getNum1() + "," + x.getNum2() + ")");
            System.out.println("");
        }

    }

    // el metodo retorna una pila de posiciones (recibe como parametro : (i,j) que se refiere a la posInicial , (k,l) se refiere a la posicionFinal, m # filas , n #columnas, M la matriz que uno le vaya a mandar
    public Stack<Posicion> caminoLaberinto(int i, int j, int k, int l, int m, int n, int[][] M) {
        Stack<Posicion> pila = new Stack();
        int posIn = M[i][j];    //posicionInicial
        pila.push(new Posicion(i, j));      // apilamos la posicion de la posicion inicial
        int posFin = M[k][l];   // creo que nunca es utilizada
        int posActual = posIn;
        int posAnterior;    //segun intellij nunca la utilizo
        Random rnd = new Random();

        while (i != k || j != l) {   //mientras la posicion actual != posicion final
            boolean sw = true;
            while (sw) {
                int aleatorio = rnd.nextInt(4) + 1;
                switch (aleatorio) {

                    case 1:
                        posAnterior = M[pila.peek().getNum1()][pila.peek().getNum2()];

                        if (i - 1 >= 0) {   //para que no se salga del limite de las filas

                            posActual = M[i - 1][j];
                            if (posActual == 0 && (i - 1 != pila.peek().getNum1() || j != pila.peek().getNum2()) && (pila.search(new Posicion(i-1,j)) ==-1 )){  //agregue acá una condicion mas
                                i = i - 1;
                                sw = false;
                                pila.push(new Posicion(i, j));

                                while ((pila.peek().getNum1() > 0 && pila.peek().getNum2() > 0) && (M[pila.peek().getNum1() - 1][pila.peek().getNum2()] + M[pila.peek().getNum1()][pila.peek().getNum2() + 1] + M[pila.peek().getNum1() + 1][pila.peek().getNum2()] + M[pila.peek().getNum1()][pila.peek().getNum2() - 1] == 3)) {

                                    M[pila.peek().getNum1()][pila.peek().getNum2()] = 1;
                                    pila.pop();
                                    posActual = M[pila.peek().getNum1()][pila.peek().getNum2()];
                                    i = pila.peek().getNum1();
                                    j = pila.peek().getNum2();
                                }
                                break;
                            }
                            else{
                                sw = true;
                                posActual = M[pila.peek().getNum1()][pila.peek().getNum2()];
                                break;
                            }
                        }
                        break;
                    case 2:
                        posAnterior = M[pila.peek().getNum1()][pila.peek().getNum2()];

                        if (j + 1 <= n) {   // para que no se salga del limite de las columnas

                            posActual = M[i][j + 1];
                            if (posActual == 0 && (i != pila.peek().getNum1() || j + 1 != pila.peek().getNum2()) && (pila.search(new Posicion(i,j+1)) ==-1 )) {  // agregue una condicion mas
                                j = j + 1;
                                sw = false;
                                pila.push(new Posicion(i, j));


                                while ((pila.peek().getNum1()>0 && pila.peek().getNum2()>0) &&(M[pila.peek().getNum1() - 1][pila.peek().getNum2()] + M[pila.peek().getNum1()][pila.peek().getNum2() + 1] + M[pila.peek().getNum1() + 1][pila.peek().getNum2()] + M[pila.peek().getNum1()][pila.peek().getNum2() - 1] == 3)) {

                                    M[pila.peek().getNum1()][pila.peek().getNum2()] = 1;
                                    pila.pop();
                                    posActual = M[pila.peek().getNum1()][pila.peek().getNum2()];
                                    i = pila.peek().getNum1();
                                    j = pila.peek().getNum2();
                                }
                                break;

                            }
                            else {
                                sw = true;
                                posActual = M[pila.peek().getNum1()][pila.peek().getNum2()];
                                break;

                            }

                        }
                        break;
                    case 3:
                        posAnterior = M[pila.peek().getNum1()][pila.peek().getNum2()];

                        if (i + 1 <= m) {  //para que no se salga del limite de las filas
                            posActual = M[i + 1][j];

                            if (posActual == 0 && (i + 1 != pila.peek().getNum1() || j != pila.peek().getNum2()) && (pila.search(new Posicion(i+1,j)) ==-1 )) { // agregue una condicion mas
                                i = i + 1;

                                sw = false;
                                pila.push(new Posicion(i, j));

                                while ((pila.peek().getNum1()>0 && pila.peek().getNum2()>0) && (M[pila.peek().getNum1() - 1][pila.peek().getNum2()] + M[pila.peek().getNum1()][pila.peek().getNum2() + 1] + M[pila.peek().getNum1() + 1][pila.peek().getNum2()] + M[pila.peek().getNum1()][pila.peek().getNum2() - 1] == 3)) {

                                    M[pila.peek().getNum1()][pila.peek().getNum2()] = 1;
                                    pila.pop();
                                    posActual = M[pila.peek().getNum1()][pila.peek().getNum2()];
                                    i = pila.peek().getNum1();
                                    j = pila.peek().getNum2();

                                }
                                break;
                            }
                            else {
                                sw = true;
                                posActual = M[pila.peek().getNum1()][pila.peek().getNum2()];
                                break;
                            }

                        }
                        break;
                    case 4:
                        posAnterior = M[pila.peek().getNum1()][pila.peek().getNum2()];

                        if (j - 1 >= 0) {   // para que no se salga del limite de las columnas
                            posActual = M[i][j - 1];
                            if (posActual == 0 && (i != pila.peek().getNum1() || j - 1 != pila.peek().getNum2()) && (pila.search(new Posicion(i,j-1)) ==-1 )) {     // agregue una condicion mas
                                j = j - 1;
                                sw = false;
                                pila.push(new Posicion(i, j));


                                while ((pila.peek().getNum1()>0 && pila.peek().getNum2()>0 )&& (M[pila.peek().getNum1() - 1][pila.peek().getNum2()] + M[pila.peek().getNum1()][pila.peek().getNum2() + 1] + M[pila.peek().getNum1() + 1][pila.peek().getNum2()] + M[pila.peek().getNum1()][pila.peek().getNum2() - 1] == 3)) {

                                    M[pila.peek().getNum1()][pila.peek().getNum2()] = 1;
                                    pila.pop();
                                    posActual = M[pila.peek().getNum1()][pila.peek().getNum2()];
                                    i = pila.peek().getNum1();
                                    j = pila.peek().getNum2();
                                }
                                break;
                            }
                            else {
                                sw = true;
                                posActual = M[pila.peek().getNum1()][pila.peek().getNum2()];
                                break;
                            }
                        }
                        break;
                }
            }

        }
        return pila;
    }

}

